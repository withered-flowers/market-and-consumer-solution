const fs = require('fs');
const Controller = require('./controllers/controller.js');

const inputArgv = process.argv;

if(inputArgv[2] === 'listAll') {
  Controller.listAll();
}
else if(inputArgv[2] === 'addPenjual') {
  Controller.addPenjual(inputArgv);
}
else if(inputArgv[2] === 'addPelanggan') {
  Controller.addPelanggan(inputArgv);
}
else if(inputArgv[2] === 'showTablePenjual') {
  Controller.showTablePenjual();
}