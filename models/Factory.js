const { PenjualEceran, PenjualGrosiran, PenjualAbalAbal } = require('./Penjual.js');

class MarketFactory {
  static createMarket(selector, id, nama, lokasi, list) {
    if (selector === 'GR') {
      return new PenjualGrosiran(id, nama, lokasi, list);
    }
    else if (selector === 'EC') {
      return new PenjualEceran(id, nama, lokasi, list);
    }
    else if (selector === 'AB') {
      return new PenjualAbalAbal(id, nama, lokasi, list);
    }
  }
}

module.exports = MarketFactory;