const fs = require('fs');

const MarketFactory = require('./Factory.js');
const Pelanggan = require('./Pelanggan.js');

class Helper {
  static bacaData() {
    let dataPenjual = JSON.parse(fs.readFileSync('./penjual.json', 'utf8'));
    let dataPembeli = JSON.parse(fs.readFileSync('./pelanggan.json', 'utf8'));

    dataPenjual = dataPenjual.map((elem) => {
      return MarketFactory.createMarket(
        elem.tipe_penjual,
        elem.id,
        elem.nama_penjual,
        elem.lokasi,
        dataPembeli
          .filter(elemPembeli => {
            return elemPembeli.id_penjual === elem.id;
          })
          .map(elemPembeli => {
            return new Pelanggan(
              elemPembeli.nama_pelanggan,
              elemPembeli.total_transaksi,
              elemPembeli.id_penjual
            )
          })
      );
    });

    return dataPenjual;
  }

  static tulisPenjual(data) {
    data = data.map(elem => {
      return {
        id: elem.id,
        nama_penjual: elem.namaPenjual,
        tipe_penjual: elem.tipePenjual,
        lokasi: elem.lokasi
      }
    });

    fs.writeFileSync(
      './penjual.json',
      JSON.stringify(data, null, 2),
      'utf8'
    );
  }

  static tulisPelanggan(data) {
    data = data.map(elem => {
      return elem.listPelanggan
    }).flat();

    fs.writeFileSync(
      './pelanggan.json',
      JSON.stringify(data, null, 2),
      'utf8'
    );
  }

  static addPenjual(inputArgv) {
    let data = this.bacaData();

    let namaPenjual = inputArgv[3];
    let selector = inputArgv[4];
    let lokasi = inputArgv[5];
    let id = data[data.length - 1].id + 1;

    let findLokasi = data.filter(elem => elem.lokasi === lokasi);

    if (findLokasi.length === 0) {
      data.push(
        MarketFactory.createMarket(
          selector,
          id,
          namaPenjual,
          lokasi,
          []
        )
      );

      this.tulisPenjual(data);

      return `Penjual ${namaPenjual} dengan id ${id} berhasil ditambahkan`;
    }
    else {
      return new Error(`Lokasi sudah ditempati oleh ${findLokasi[0].namaPenjual}, gunakan lokasi lain`);
    }
  }

  static addPelanggan(inputArgv) {
    let data = this.bacaData();

    let idPenjual = Number(inputArgv[3]);
    let namaPelanggan = inputArgv[4];
    let totalTransaksi = inputArgv[5];

    let selectedPenjual = data.filter(elem => elem.id === idPenjual);

    if (selectedPenjual.length > 0) {
      let maxPelanggan = selectedPenjual[0].maxPelanggan;

      if (selectedPenjual[0].listPelanggan.length + 1 > maxPelanggan) {
        return new Error(`Penjual dengan id ${idPenjual} sudah mencapai limit Pelanggan`);
      }

      else {
        let index = data.findIndex((elem) => {
          return elem.id === idPenjual;
        });

        data[index].listPelanggan.push(
          new Pelanggan(
            namaPelanggan, totalTransaksi, idPenjual
          )
        );

        this.tulisPelanggan(data);

        return `Pelanggan ${namaPelanggan} berlangganan pada Penjual ${data[index].namaPenjual}`;
      }
    }
    else {
      return new Error(`Penjual dengan id ${idPenjual} tidak ditemukan`);
    }
  }

  static tablePenjual() {
    let data = this.bacaData();

    data = data.map(elem => {
      let totalTransaksi = 0;

      elem.listPelanggan.forEach(elem => {
        totalTransaksi += elem.totalTransaksi;
      })

      return {
        id: elem.id,
        namaPenjual: elem.namaPenjual,
        tipePenjual: elem.tipePenjual,
        lokasi: elem.lokasi,
        jumlahPelanggan: elem.listPelanggan.length,
        totalTransaksi: totalTransaksi
      };
    });

    return data;
  }
}

module.exports = Helper;