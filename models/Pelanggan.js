class Pelanggan {
  constructor(nama, total, id) {
    this.namaPelanggan = nama;
    this.totalTransaksi = total;
    this.idPenjual = id;
  }
}

module.exports = Pelanggan;