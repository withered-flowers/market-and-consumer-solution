class Penjual {
  constructor(id, nama, tipe, lokasi, max, list) {
    this.id = id;
    this.namaPenjual = nama;
    this.tipePenjual = tipe;
    this.lokasi = lokasi;
    this.maxPelanggan = max;
    this.listPelanggan = list;
  }
}

class PenjualGrosiran extends Penjual {
  constructor(id, nama, lokasi, list) {
    super(id, nama, 'GR', lokasi, 3, list);
  }
}

class PenjualEceran extends Penjual {
  constructor(id, nama, lokasi, list) {
    super(id, nama, 'EC', lokasi, 2, list);
  }
}

class PenjualAbalAbal extends Penjual {
  constructor(id, nama, lokasi, list) {
    super(id, nama, 'AB', lokasi, 1, list);
  }
}

module.exports = {
  Penjual, PenjualGrosiran, PenjualEceran, PenjualAbalAbal
};