const Helper = require('../models/Helper.js');

class Controller {
  static listAll() {
    let data = Helper.bacaData();

    console.log(data);
  }

  static addPenjual(inputArgv) {
    let data = Helper.addPenjual(inputArgv);

    if(data instanceof Error) {
      console.error(data.stack);
    }
    else {
      console.log(data);
    }
  }

  static addPelanggan(inputArgv) {
    let data = Helper.addPelanggan(inputArgv);
    
    if(data instanceof Error) {
      console.error(data.stack);
    }
    else {
      console.log(data);
    } 
  }

  static showTablePenjual() {
    let data = Helper.tablePenjual();
    console.table(data);
  }
}

module.exports = Controller;